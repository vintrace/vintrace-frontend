//react
import { useEffect } from "react";
//mui
import { Box, Typography, IconButton, Grid } from "@mui/material";
import { ArrowBack, ModeEdit } from "@mui/icons-material";
import { styled } from "@mui/material/styles";
//components
import Tab from "../layouts/Tab";
//react
import { Link } from "react-router-dom";
//redux
import { useDispatch, useSelector } from "../redux/store";
import { getWineDetail } from "../redux/slices/wine";

/*-------------------------------------------*/

const RootStyle = styled(Box)(() => ({
  display: "flex",
  flexDirection: "column",
  padding: "5% 20%",
}));

const HeadingStyle = styled(Typography)`
  font-weight: 300;
  ${(props) => props.theme.breakpoints.up("xs")} {
    font-size: 16px;
  }
  ${(props) => props.theme.breakpoints.up("sm")} {
    font-size: 30px;
  }
  ${(props) => props.theme.breakpoints.up("md")} {
    font-size: 40px;
  }
`;

const Subtitle = styled(Typography)`
  font-weight: 300;
  ${(props) => props.theme.breakpoints.up("xs")} {
    font-size: 12px;
  }
  ${(props) => props.theme.breakpoints.up("sm")} {
    font-size: 16px;
  }
  ${(props) => props.theme.breakpoints.up("md")} {
    font-size: 21px;
  }
`;

const AttributeGrid = styled(Grid)`
  display: flex;
  justify-content: space-between;
  ${(props) => props.theme.breakpoints.up("xs")} {
    font-size: 12px;
  }
  ${(props) => props.theme.breakpoints.up("sm")} {
    font-size: 16px;
  }
  ${(props) => props.theme.breakpoints.up("md")} {
    font-size: 18px;
  }
`;

const EditButton = styled(IconButton)(() => ({
  background: "#00ADA8",
  color: "#FFF",
  fontSize: "20px",
  ":hover" :{
    background: "#00ADA8"
  }
}));

/*-------------------------------------------*/

function ComponentDetail() {
  const dispatch = useDispatch();
  const lotCode = localStorage.getItem("lotcode");
  const { wine } = useSelector((state) => state.wine);

  useEffect(() => {
    if (lotCode) {
      dispatch(getWineDetail(lotCode));
    }
  }, [dispatch, lotCode]);

  return (
    <RootStyle>
      <Link to={"/"}>
        <IconButton id="cypress-detail-arrow-back-icon" sx={{ width: "30px" }}>
          <ArrowBack />
        </IconButton>
      </Link>

      <Box sx={{ display: "flex", justifyContent: "space-between" }}>
        <Box sx={{ display: "flex", alignItems: "center" }}>
          <img
            width={30}
            height={30}
            src={require("../images/bigW.png")}
            alt=""
          />
          <HeadingStyle id="cypress-detail-title" variant="h1">
            {wine?.lotCode}
          </HeadingStyle>
        </Box>
        <EditButton aria-label="delete">
          <ModeEdit fontSize="inherit"/>
        </EditButton>
      </Box>
      <Subtitle>{wine?.description}</Subtitle>
      <Box sx={{ p: "21px 0px 29px" }}>
        <AttributeGrid>
          <Grid xs={3}>
            <Typography sx={{ fontWeight: 400 }}>Volumn</Typography>
          </Grid>
          <Typography sx={{ fontWeight: 300 }}>{wine?.volume}L</Typography>
        </AttributeGrid>
        <AttributeGrid>
          <Grid xs={3}>
            <Typography sx={{ fontWeight: 400 }}>Tank code</Typography>
          </Grid>
          <Typography sx={{ fontWeight: 300 }}>{wine?.tankCode}</Typography>
        </AttributeGrid>
        <AttributeGrid>
          <Grid xs={5}>
            <Typography sx={{ fontWeight: 400 }}>Product state</Typography>
          </Grid>
          <Typography sx={{ fontWeight: 300 }}>{wine?.productState}</Typography>
        </AttributeGrid>
        <AttributeGrid>
          <Grid xs={3}>
            <Typography sx={{ fontWeight: 400 }}>Owner</Typography>
          </Grid>
          <Typography sx={{ fontWeight: 300 }}>{wine?.ownerName}</Typography>
        </AttributeGrid>
      </Box>
      <Tab />
    </RootStyle>
  );
}

export default ComponentDetail;
