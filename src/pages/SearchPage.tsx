//react
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
//mui
import {
  Box,
  Typography,
  Paper,
  InputBase,
  IconButton,
  Button,
} from "@mui/material";
import { Search, Close } from "@mui/icons-material";
import { styled } from "@mui/material/styles";
//redux
import { useDispatch, useSelector } from "../redux/store";
import { getWineList, getWineDetail } from "../redux/slices/wine";
import { Wine } from "../type";
/*-------------------------------------------*/

const SearchStyle = styled(Box)(() => ({
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  paddingTop: "7%",
}));

const ButtonStyle = styled(Button)(() => ({
  display: "flex",
  flexDirection: "column",
  alignItems: "start",
  width: 632,
  height: 48,
  borderRadius: 0,
  backgroundColor: "#FFF",
  color: "inherit",
  ":hover": {
    background: "lightgrey",
    boxShadow: "none",
  },
}));

const TextStyle = styled(Typography)(() => ({
  fontWeight: 300,
  fontSize: "26px",
  lineHeight: "32.5px",
  padding: "0px 16px",
}));

const Title = styled(Typography)(() => ({
  fontWeight: 300,
  fontSize: "15px",
  lineHeight: "18px",
}));

const Description = styled(Typography)(() => ({
  fontWeight: 300,
  fontSize: "13px",
  lineHeight: "18px",
  textTransform: "capitalize",
}));

/*-------------------------------------------*/

function SearchPage() {
  const [searchValue, setSearchValue] = useState("");
  const [searchList, setSearchList] = useState<Wine[]>([]);
  const nav = useNavigate();
  // Redux hooks
  const dispatch = useDispatch();
  const { wineList } = useSelector((state) => state.wine);

  const handleChange = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const targetValue = event.target.value.toUpperCase();
    setSearchValue(targetValue);
  };

  const handleSearchList = (
    event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const targetValue = event.target.value.toUpperCase();
    if (wineList) {
      let searchItems: Wine[] = [];

      const filteredItems = wineList.filter((wine) => {
        const lotCode = wine.lotCode.toUpperCase();
        const description = wine.description
          ? wine.description.toUpperCase()
          : "";
        if (
          lotCode.indexOf(targetValue) !== -1 ||
          description.indexOf(targetValue) !== -1
        )
          return true;
        return false;
      });

      if (targetValue !== "") {
        searchItems = filteredItems;
      } else if (targetValue === "") {
        searchItems = [];
      }

      setSearchList(searchItems);
    }
  };

  const handleDelete = () => {
    setSearchValue("");
    setSearchList([]);
  };

  const getDetail = async (lotCode: string) => {
    await dispatch(getWineDetail(lotCode));
    nav("/detail");
  };

  useEffect(() => {
    dispatch(getWineList());
  }, [dispatch]);

  return (
    <SearchStyle>
      <Box sx={{ display: "flex", justifyContent: "center", p: "24px 0px" }}>
        <TextStyle variant="h3">Wine search</TextStyle>
        <img src={require("../images/wineGlass.png")} alt="" />
      </Box>
      <Paper
        component="form"
        sx={{
          p: "2px 4px",
          display: "flex",
          alignItems: "center",
          width: 624,
          height: 48,
          borderRadius: 0,
        }}
      >
        <IconButton type="submit" sx={{ p: "10px" }} aria-label="search">
          <Search />
        </IconButton>
        <InputBase
          id="cypress-search"
          sx={{ ml: 1, flex: 1 }}
          placeholder="Search by lot code and description......"
          value={searchValue}
          onChange={(event) => {
            handleChange(event);
            handleSearchList(event);
          }}
        />
        <IconButton
          sx={{ p: "10px", display: searchValue ? "block" : "none" }}
          aria-label="delete"
          onClick={handleDelete}
        >
          <Close />
        </IconButton>
      </Paper>
      {searchList.map((wine) => (
        <ButtonStyle
          id="cypress-shown-button"
          variant="contained"
          onClick={() => {
            getDetail(wine.lotCode);
          }}
        >
          <Title>{wine.lotCode}</Title>
          <Description>{wine.description}</Description>
        </ButtonStyle>
      ))}
    </SearchStyle>
  );
}

export default SearchPage;
