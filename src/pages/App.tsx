//router
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
//pages
import SearchPage from "./SearchPage";
import ComponentDetail from "./ComponentDetail";
//redux
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/lib/integration/react";
import { store, persistor } from "../redux/store";

/*-------------------------------------------*/

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <Router>
          <Routes>
            <Route path="/" element={<SearchPage />} />
            <Route path="/detail" element={<ComponentDetail />} />
          </Routes>
        </Router>
      </PersistGate>
    </Provider>
  );
}

export default App;
