export type Wine = {
  lotCode: string,
  description: string,
  volume: number,
  tankCode: string,
  productState: string,
  ownerName: string,
  components: Component[],
};

export type WineState = {
  isLoading: boolean,
  error: Error | string | null,
  wineList: Wine[] | null,
  wine: Wine | null,
  breakdown: Breakdown | null,
};

export type Component = {
  percentage: number,
  year: number,
  variety: string,
  region: string,
};

export type Breakdown = {
  breakDownType: string,
  breakdown: BreakdownComponent[],
};

export type BreakdownComponent= {
    percentage: string,
    key: string,
}
