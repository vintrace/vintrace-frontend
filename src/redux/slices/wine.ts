import { createSlice } from "@reduxjs/toolkit";
import axios from "../../utils/axios";
import { Wine, WineState, Breakdown } from "../../type";
//
import { dispatch } from "../store";
// ----------------------------------------------------------------------

const initialState: WineState = {
  isLoading: false,
  error: null,
  wineList: [],
  wine: null,
  breakdown: null,
};

const slice = createSlice({
  name: "wine",
  initialState,
  reducers: {
    // START LOADING
    startLoading(state) {
      state.isLoading = true;
    },

    // HAS ERROR
    hasError(state, action) {
      state.isLoading = false;
      state.error = action.payload;
    },

    // GET WINE LIST
    getWineList(state, action) {
      state.isLoading = false;
      state.wineList = action.payload.data;
    },

    //GET WINE DETAILS
    getWineDetailSuccess(state, action) {
      state.isLoading = false;
      state.wine = action.payload.data;
    },

    //UPDATE WINE
    updateWineSuccess(state, action) {
      state.isLoading = false;
      state.wine = action.payload.data;
    },

    //GET BREAKDOWN
    getBreakdownSuccess(state, action) {
      state.isLoading = false;
      state.breakdown = action.payload.data;
    },
  },
});

// Reducer
export default slice.reducer;

// Actions

// ----------------------------------------------------------------------

export function getWineList() {
  return async () => {
    dispatch(slice.actions.startLoading());
    try {
      const response: {
        data: {
          data: Wine[];
        };
      } = await axios.get("/api/wineList");
      dispatch(slice.actions.getWineList(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

export function getWineDetail(lotCode: string) {
  return async () => {
    dispatch(slice.actions.startLoading());
    try {
      const response: {
        data: {
          data: Wine;
        };
      } = await axios.get(`/api/wine/${lotCode}`);
      localStorage.setItem("lotcode", lotCode);
      dispatch(slice.actions.getWineDetailSuccess(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}

export function getBreakdown(lotCode: string, breakdownBy: string) {
  return async () => {
    dispatch(slice.actions.startLoading());
    try {
      const response: {
        data: {
          data: Breakdown;
        };
      } = await axios.get(`/api/breakdown/${breakdownBy}/${lotCode}`);
      dispatch(slice.actions.getBreakdownSuccess(response));
    } catch (error) {
      dispatch(slice.actions.hasError(error));
    }
  };
}
