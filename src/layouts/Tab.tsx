//react
import { useState, useEffect, ReactNode } from "react";
//mui
import { Tab, Tabs, Typography, Box, CircularProgress } from "@mui/material";
import { styled } from "@mui/material/styles";
//table
import Table from "./Table";
//redux
import { useDispatch, useSelector } from "../redux/store";
import { getBreakdown } from "../redux/slices/wine";

/*-------------------------------------------*/

const TabStyle = styled(Box)(() => ({
  borderBottom: 1,
  borderColor: "divider",
  color: "#00ADA !important",
}));

/*-------------------------------------------*/

type TabPanelProps = {
  children: ReactNode;
  value: number;
  index: number;
};

/*-------------------------------------------*/

function TabPanel(props: TabPanelProps) {
  const { children, value, index } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
    >
      {value === index && (
        <Box sx={{ p: "11px 0px" }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

function a11yProps(index: number) {
  return {
    id: `cypress-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

function ComponentDetail() {
  const [tab, setTab] = useState(0);
  const [breakdownBy, setBreakdownBy] = useState("year");
  const lotCode = localStorage.getItem("lotcode");
  const dispatch = useDispatch();
  const { breakdown, isLoading } = useSelector((state) => state.wine);

  const handleChange = (
    event: React.SyntheticEvent<Element, Event>,
    newValue: number
  ) => {
    setTab(newValue);
    switch (newValue) {
      case 0:
        setBreakdownBy("year");
        break;
      case 1:
        setBreakdownBy("variety");
        break;
      case 2:
        setBreakdownBy("region");
        break;
      case 3:
        setBreakdownBy("year-variety");
        break;
    }
  };

  useEffect(() => {
    if (lotCode) {
      dispatch(getBreakdown(lotCode, breakdownBy));
    }
  }, [dispatch, breakdownBy, lotCode]);

  if (isLoading) {
    return (
      <Box sx={{ width: "100%" }}>
        <TabStyle>
          <Tabs
            value={tab}
            onChange={handleChange}
            aria-label="basic tabs example"
            TabIndicatorProps={{ style: { backgroundColor: "#00ADA8" } }}
          >
            <Tab label="Year" {...a11yProps(0)} />
            <Tab label="Variety" {...a11yProps(1)} />
            <Tab label="Region" {...a11yProps(2)} />
            <Tab label="Year & Variety" {...a11yProps(3)} />
          </Tabs>
        </TabStyle>
        <TabPanel value={tab} index={0}>
          <CircularProgress size={24} />
        </TabPanel>
      </Box>
    );
  }

  if (breakdown) {
    return (
      <Box sx={{ width: "100%" }}>
        <TabStyle>
          <Tabs
            value={tab}
            onChange={handleChange}
            aria-label="basic tabs example"
            TabIndicatorProps={{ style: { backgroundColor: "#00ADA8" } }}
          >
            <Tab label="Year" {...a11yProps(0)} />
            <Tab label="Variety" {...a11yProps(1)} />
            <Tab label="Region" {...a11yProps(2)} />
            <Tab label="Year & Variety" {...a11yProps(3)} />
          </Tabs>
        </TabStyle>
        <TabPanel value={tab} index={0}>
          <Table
            key={"year"}
            breakDownType={breakdownBy}
            breakdown={breakdown.breakdown}
          />
        </TabPanel>
        <TabPanel value={tab} index={1}>
          <Table
            key={"variety"}
            breakDownType={breakdownBy}
            breakdown={breakdown.breakdown}
          />
        </TabPanel>
        <TabPanel value={tab} index={2}>
          <Table
            key={"region"}
            breakDownType={breakdownBy}
            breakdown={breakdown.breakdown}
          />
        </TabPanel>
        <TabPanel value={tab} index={3}>
          <Table
            key={"year-variety"}
            breakDownType={breakdownBy}
            breakdown={breakdown.breakdown}
          />
        </TabPanel>
      </Box>
    );
  } else {
    return <Typography>Something wrong happens</Typography>;
  }
}

export default ComponentDetail;
