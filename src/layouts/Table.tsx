//react
import { useState, useEffect } from "react";
//mui
import {
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Paper,
  Button,
  Typography,
} from "@mui/material";
import { KeyboardArrowDown, KeyboardArrowUp } from "@mui/icons-material";
import { styled } from "@mui/material/styles";
//type
import { Breakdown, BreakdownComponent } from "../type";

/*-------------------------------------------*/

const ButtonStyle = styled(Button)(() => ({
  display: "flex",
  color: "#00928D",
  width: "inherit",
  height: 48,
  borderRadius: 0,
  backgroundColor: "#FFF",
  ":hover": {
    background: "lightgrey",
    boxShadow: "none",
  },
}));

const TextButtonStyle = styled(Typography)(() => ({
  color: "#00928D",
  fontWeight: 400,
  fontSize: "11px",
  lineHeight: "16px",
}));

export function capitalizeFirstLetter(word: string) {
  const charList = word.split("");
  let result = charList[0].toUpperCase();
  for (let i = 1; i < charList.length; i++) {
    result += word[i];
  }
  return result;
}

export function returnFirstFiveElement(list: BreakdownComponent[]) {
  let initialShow: BreakdownComponent[] = [];
  if (list.length > 5) {
    for (let i = 0; i < 5; i++) {
      initialShow.push(list[i]);
    }
  } else {
    initialShow = list;
  }
  return initialShow;
}

function ComponentTable(props: Breakdown) {
  const [showMore, setShowMore] = useState(true);
  const needShowMoreButton = props.breakdown.length > 5;
  const [initialShow, setInitialShow] = useState<BreakdownComponent[]>([]);
  const [shownBreakdown, setShownBreakDown] = useState<BreakdownComponent[]>(
    returnFirstFiveElement(props.breakdown)
  );

  const handleShowMore = () => {
    setShowMore(!showMore);
    if (showMore) {
      setShownBreakDown(props.breakdown);
    } else {
      setShownBreakDown(initialShow);
    }
  };

  // eslint-disable-next-line react-hooks/exhaustive-deps
  useEffect(() => {
    const initialShow = returnFirstFiveElement(props.breakdown);
    setInitialShow(initialShow);
  });

  return (
    <TableContainer component={Paper}>
      <Table aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell id="cypress-table-header">
              {capitalizeFirstLetter(props.breakDownType)}
            </TableCell>
            <TableCell align="right">Percentage</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {shownBreakdown.map((component) => (
            <TableRow
              id="cypress-table-row"
              key={component.key}
              sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
            >
              <TableCell component="th" scope="row">
                {component.key}
              </TableCell>
              <TableCell align="right">{component.percentage}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
      <ButtonStyle
        id="cypress-table-show-more-button"
        style={{ display: needShowMoreButton ? "flex" : "none" }}
        variant="contained"
        onClick={handleShowMore}
      >
        <TextButtonStyle>
          {showMore ? "Show more" : "Show less"}
        </TextButtonStyle>
        {showMore ? <KeyboardArrowDown /> : <KeyboardArrowUp />}
      </ButtonStyle>
    </TableContainer>
  );
}

export default ComponentTable;
