import { capitalizeFirstLetter } from "../layouts/Table";

describe("Component Detail Page", () => {
  it("should return expected result", () => {
    expect(capitalizeFirstLetter("this is for jest testing")).toMatchSnapshot();
  });
});
