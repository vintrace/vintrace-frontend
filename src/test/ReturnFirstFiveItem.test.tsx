import { returnFirstFiveElement } from "../layouts/Table";

describe("Component Detail Page", () => {
  it("should return expected result", () => {
    const testComponent = [
      {
        percentage: "1",
        key: "test",
      },
      {
        percentage: "2",
        key: "test",
      },
      {
        percentage: "3",
        key: "test",
      },
      {
        percentage: "4",
        key: "test",
      },
      {
        percentage: "5",
        key: "test",
      },
      {
        percentage: "6",
        key: "test",
      },
    ];
    expect(returnFirstFiveElement(testComponent)).toMatchInlineSnapshot(`
Array [
  Object {
    "key": "test",
    "percentage": "1",
  },
  Object {
    "key": "test",
    "percentage": "2",
  },
  Object {
    "key": "test",
    "percentage": "3",
  },
  Object {
    "key": "test",
    "percentage": "4",
  },
  Object {
    "key": "test",
    "percentage": "5",
  },
]
`);
  });
});
