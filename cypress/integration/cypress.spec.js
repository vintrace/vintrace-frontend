/* eslint-disable no-undef */
describe("End to end test", () => {
    beforeEach(() => {
        cy.visit("/");
    });

    it("should display search page", () => {
        cy.get("h3").should("contain", "Wine search");
        cy.get("#cypress-search").focus().clear().type("11YVCHAR001");
        cy.get("#cypress-shown-button").click();
        cy.get("#cypress-detail-title").should("contain", "11YVCHAR001");
        cy.get('#cypress-tab-0').click();
        cy.get('#cypress-table-header').should("contain", "Year")
        cy.get('#cypress-tab-1').click();
        cy.get('#cypress-table-header').should("contain", "Variety");
        cy.get('#cypress-tab-2').click();
        cy.get('#cypress-table-header').should("contain", "Region")
        cy.get('#cypress-tab-3').click();
        cy.get('#cypress-table-header').should("contain", "Year-variety");
        cy.get('#cypress-detail-arrow-back-icon').click();
        cy.get("#cypress-search").focus().clear().type("11YVCHAR002");
        cy.get("#cypress-shown-button").click();
        cy.get("#cypress-detail-title").should("contain", "11YVCHAR002");
        cy.get('#cypress-tab-0').click();
        cy.get('#cypress-table-header').should("contain", "Year")
        cy.get('#cypress-tab-1').click();
        cy.get('#cypress-table-header').should("contain", "Variety");
        cy.get('#cypress-tab-2').click();
        cy.get('#cypress-table-header').should("contain", "Region")
        cy.get('#cypress-tab-3').click();
        cy.get('#cypress-table-header').should("contain", "Year-variety");
        cy.get('#cypress-detail-arrow-back-icon').click();
        cy.get("#cypress-search").focus().clear().type("15MPPN002-VK");
        cy.get("#cypress-shown-button").click();
        cy.get("#cypress-detail-title").should("contain", "15MPPN002-VK");
        cy.get('#cypress-tab-0').click();
        cy.get('#cypress-table-header').should("contain", "Year")
        cy.get('#cypress-tab-1').click();
        cy.get('#cypress-table-header').should("contain", "Variety");
        cy.get('#cypress-tab-2').click();
        cy.get('#cypress-table-header').should("contain", "Region")
        cy.get('#cypress-tab-3').click();
        cy.get('#cypress-table-header').should("contain", "Year-variety");
        cy.get('#cypress-table-show-more-button').should("contain", "Show more").click()
        cy.get('#cypress-table-show-more-button').should("contain", "Show less").click()
        cy.get('#cypress-detail-arrow-back-icon').click();
    });
});
